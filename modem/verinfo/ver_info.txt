{
    "Image_Build_IDs": {
        "adsp": "ADSP.VT.5.4.1-00340-KAMORTA-4", 
        "apps": "LA.UM.9.15.r1-02000-KAMORTA.0-1", 
        "apps_qssi": "LA.QSSI.11.0.r1-07900-qssi.0-2", 
        "boot": "BOOT.XF.4.1-00206-KAMORTALAZ-1", 
        "btfm": "BTFM.CHE.2.1.5-00261-QCACHROMZ-1", 
        "btfm_cmc": "BTFM.CMC.1.2.0-00186-QCACHROMZ-1", 
        "cdsp": "CDSP.VT.2.4.1-00201-KAMORTA-1", 
        "common": "Kamorta.LA.1.0.1-00050-STD.PROD-2", 
        "glue": "GLUE.KAMORTA_LA.1.1-00016-NOOP_TEST-1", 
        "modem": "MPSS.HA.1.0-00577-KAMORTA_GEN_PACK-1", 
        "rpm": "RPM.BF.1.10-00170-KAMORTAAAAAANAZR-1", 
        "tz": "TZ.XF.5.1-00870-KAMORTAAAAAANAZT-1", 
        "tz_apps": "TZ.APPS.2.0-00116-KAMORTAAAAAANAZT-1", 
        "video": "VIDEO.VE.6.0-00035-PROD-1", 
        "wlan": "WLAN.HL.3.2.4-00730.3-QCAHLSWMTPLZ-1"
    }, 
    "Metabuild_Info": {
        "Meta_Build_ID": "Kamorta.LA.1.0.1-00050-STD.PROD-2", 
        "Product_Flavor": "['asic']", 
        "Time_Stamp": "2023-03-18 15:38:42"
    }, 
    "Version": "1.0"
}